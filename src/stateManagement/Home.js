import React from 'react';
import {View, Text, ScrollView, TouchableOpacity} from 'react-native';
import {useSelector} from 'react-redux';

const HomeScreen = () => {
  const {isLoggedIn, userData} = useSelector(state => state.auth);
  console.log('cek userData', userData);
  return (
    <View>
      <Text> Ini Home</Text>
    </View>
  );
};

export default HomeScreen;
